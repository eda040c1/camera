package own.client;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class ClientMonitor{

	/** Server modes */
	private static final int IDLE = 0;
	private static final int MOVIE = 1;

	private int mode;

	private boolean picPar = true;
	private String hostName;					//Used for notifying GUI were motion detection came from

	/** Client modes */
	private static final int syncAuto = 0;
	private static final int syncForced = 1;
	private static final int asyncForced = 2;
	private int asyncCount=0;
	private int syncMode = syncAuto;			// Either syncAuto, syncForced or asyncForced
	private boolean sync;						// To se if sync or async while syncAuto is engaged 

	/** Allocated Server Data */
	private byte data1[];
	private byte data2[];

	private long timestamp1;
	private long timestamp2;

	private double delay1;
	private double delay2;

	private boolean newPic1 = false;		// Indicates new data from cam1
	private boolean newPic2 = false;		// Indicates new data from cam2

	/** GUI Data */

	private boolean syncFlag;
	private boolean autoFlag;


	/** Mode flag */
	private boolean modeFlag = false;
	private int modeIncrement = 0;

	/** Monitor Data */
	private final double threshold=0.2;		//Threshold timewindow
	private double picDiff;					// Delay data for GUI to fetch in getDelay method
	private long startTime;
	private ArrayList<byte[]> stream;

	/** Client Send Data */
	int lastThread;

	/** Thread data */
	// Final host names and ints for 4 cameras
	private final String camHost1 = "argus-1.student.lth.se";
	private final int fetch1 = 6000;
	private final int send1 = 6001;
	private final String camHost2 = "argus-2.student.lth.se";
	private final int fetch2 = 7000;
	private final int send2 = 7001;
	private final String camHost3 = "argus-3.student.lth.se";
	private final int fetch3 = 8000;
	private final int send3 = 8001;
	private final String camHost4 = "argus-4.student.lth.se";
	private final int fetch4 = 9000;
	private final int send4 = 9001;

	// Host name and port nr for the cameras current in use
	private  String leftHost;
	private int leftFetch;
	private int leftSend;
	private String rightHost;
	private int rightFetch;
	private int rightSend;

	// Flags for connecting and disconnecting left or right cam
	private boolean connectLeft = false;
	private boolean connectRight = false;
	private int portInc = 0;

	public ClientMonitor(){
		mode = IDLE;
		sync = true; // Vad ska den vara fr�n b�rjan??

		stream = new ArrayList<byte[]>(2);					// 2 platser byte[]-buffer

		hostName = "";
	}

	/** Receive data from client fetch- threads and distinguishes if from cam1 or cam2 to put
	 * in separate new byte[] for the GUI to fetch w. getData() 
	 * data contains long timestamp + image data with at total length of input "size"
	 * */
	public synchronized boolean putData(int portNbr, byte [] data, int size){
		boolean ret = true;

		//		System.out.println("Entered put data");
		if(portNbr==leftFetch){									//verifies camera

			// ByteBuffer is used for decompositing the byte[] called data
			ByteBuffer tempJpeg = ByteBuffer.allocate(size);
			tempJpeg = ByteBuffer.wrap(data);
			timestamp1 = tempJpeg.getLong();	


			data1=new byte[tempJpeg.remaining()];
			// change current sync w. sync()

			tempJpeg.get(data1, 0, size); //size-(Long.BYTES)
			newPic1 = true;
			ret = connectLeft;
		}

		if(portNbr==rightFetch){

			ByteBuffer tempJpeg = ByteBuffer.allocate(size);
			tempJpeg = ByteBuffer.wrap(data);

			timestamp2 = tempJpeg.getLong();

			data2=new byte[tempJpeg.remaining()];
			tempJpeg.get(data2, 0, size);
			newPic2 = true;
			ret = connectRight;
		}
		notifyAll();
		return ret;
	}

	/** Used by Gui */
	public synchronized ArrayList<byte[]> getData(){
		while(!newPic1 && !newPic2){					// no new pictures
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		startTime=System.currentTimeMillis();
		stream.add(0, null);
		stream.add(1, null);
		switch (syncMode){
		default: break;
		case syncAuto: 
			if(sync){
				if(newPic1 && newPic2 && picPar){	// Two pics from the same couple, send oldest pic without delay
					picDiff=Math.abs(delay1-delay2);
					if(picDiff<threshold){
						asyncCount=0;
					} else {
						asyncCount++;
					}
					if(timestamp1<timestamp2){
						stream.add(0, data1);
						newPic1=false;
						delay1=(int)(System.currentTimeMillis()-timestamp1);
					} else {
						stream.add(1, data2);
						newPic2=false;
						delay2=(int)(System.currentTimeMillis()-timestamp2);
					}
					picPar=false;

				} else if (!picPar){			// Always a pair w previous picture == already sent out 1 picture
					if(timestamp1<timestamp2){
						stream.add(0, data1);
						newPic1=false;
						delay1=(int)(System.currentTimeMillis()-timestamp1);

					} else {
						stream.add(1, data2);
						newPic2=false;
						delay2=(int)(System.currentTimeMillis()-timestamp2);
					}
					picPar=true;
					try {
						wait((long)picDiff-(System.currentTimeMillis()-startTime));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					if(newPic1){							// If pic one is new
						try {
							wait(200-(System.currentTimeMillis()-startTime));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(newPic2){
							picPar=false;					// to guarantee to send pic2 in the pair next loop
							asyncCount=0;
						} else {
							asyncCount++;
						}
						stream.add(0, data1);
						newPic1=false;
						delay1=(int)(System.currentTimeMillis()-timestamp1);

					} else { 								// If pic 2 is new
						try {
							wait(200-(System.currentTimeMillis()-startTime));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(newPic1){
							picPar=false;					// to guarantee to send pic2 in the pair next loop
							asyncCount=0;
						} else {
							asyncCount++;
						}
						stream.add(1, data2);
						newPic2=false;
						delay2=(int)(System.currentTimeMillis()-timestamp2);
					}
				}
			} else {										// Async mode
				if(newPic1 && newPic2){						// If 2 new pics
					if(timestamp1<timestamp2){
						stream.add(0, data1);
						newPic1=false;
						delay1=(int)(System.currentTimeMillis()-timestamp1);
					} else {
						stream.add(1, data2);
						newPic2=false;
						delay2=(int)(System.currentTimeMillis()-timestamp2);
					}
				} else {
					if(newPic1){							// If pic 1 is new
						stream.add(0, data1);
						newPic1=false;
						delay1=(int)(System.currentTimeMillis()-timestamp1);
					} else {								// If pic 2 is new
						stream.add(1, data2);
						newPic2=false;
						delay2=(int)(System.currentTimeMillis()-timestamp2);
					}
				}
			}
			if(asyncCount==100){sync=false; asyncCount=0;} // What's resonable for couple of seconds
			break;

		case syncForced:
			sync=true;
			if(newPic1 && newPic2 && picPar){	// Two pics from the same couple, send oldest pic without delay
				picDiff=Math.abs(delay1-delay2);
				if(picDiff<threshold){
					asyncCount=0;
				} else {
					asyncCount++;
				}
				if(timestamp1<timestamp2){
					stream.add(0, data1);
					newPic1=false;

				} else {
					stream.add(1, data2);
					newPic2=false;
				}
				picPar=false;

			} else if (!picPar){			// Always a pair w previous picture == already sent out 1 picture
				if(timestamp1<timestamp2){
					stream.add(0, data1);
					newPic1=false;

				} else {
					stream.add(1, data2);
					newPic2=false;
				}
				picPar=true;
				try {
					wait((long)picDiff-(System.currentTimeMillis()-startTime));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				if(newPic1){							// If pic 1 is new
					try {
						wait(200-(System.currentTimeMillis()-startTime));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(newPic2){						// If pic 2 is recived after 200 ms
						picPar=false;					// to guarantee to send pic2 in the pair next loop
						asyncCount=0;
					} else {
						asyncCount++;
					}
					stream.add(0, data1);
					newPic1=false;

				} else { 								// If pic 2 is new
					try {
						wait(200-(System.currentTimeMillis()-startTime));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(newPic1){						// If pic 1 is recived after 200 ms
						picPar=false;					// to guarantee to send pic2 in the pair next loop
					} else {
						asyncCount++;
					}
					stream.add(1, data2);
					newPic2=false;
				}
			}
			break;


		case asyncForced:
			sync=false;
			if(newPic1 && newPic2){
				if(timestamp1<timestamp2){
					stream.add(0, data1);
					newPic1=false;

				} else {
					stream.add(1, data2);
					newPic2=false;
				}
			} else {
				if(newPic1){
					stream.add(0, data1);
					newPic1=false;
				} else {
					stream.add(1, data2);
					newPic2=false;
				}
			}
			break;
		}
		return stream;
	}

	/** Returns current delay between capturing and displaying times for a/a pair of pic(s) */
	public synchronized double getDelay(int cam){			//maybe has to be done in GUI for proper time delay?
		if(cam==0){return delay1;} else {
			return delay2;}		
	}


	/** Called by ClientSend, used for notifying camera about motion detection */  
	public synchronized byte[] changeMode(int portNbr, String location){
		while((!modeFlag || lastThread==portNbr) && ((portNbr == leftSend && connectLeft) || (portNbr == rightSend && connectRight))){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Modefalg "+ modeFlag + lastThread + " " + portNbr + " ConnectLeft " + connectLeft + " " + leftSend + " ConnecRight " + connectRight + " " + rightSend);

		System.out.println("Mode " + mode);
		byte[] ret;

		if (location.equals("Left") && !connectLeft){
			ret = null;
		} else if (location.equals("Right") && !connectRight){
			ret = null;
		} else {

			lastThread=portNbr;
			modeIncrement++;
			if(modeIncrement==2){
				modeFlag = false;
				modeIncrement=0;
			}

			ret = intToByte(mode);
		}

		notifyAll();
		return ret;
	}

	public synchronized byte[] startMode(){
		byte[] ret = intToByte(mode);
		return ret;
	}

	public synchronized String cameraMode(){
		String ret;
		if (mode == MOVIE){
			ret = "Movie ";
		} else {
			ret = "Idle";
		}
		return ret;
	}
	public synchronized String hostName(String location){
		if(location.equals("Left")){
			return leftHost;
		}else if (location.equals("Right")){
			return rightHost;
		}else{
			return hostName;
		}

	}

	public synchronized boolean isAuto(){
		return syncMode==syncAuto;
	}

	public synchronized boolean isSync(){
		return sync;

	}


	// *********************** Commands from GUI *************************

	/** Called from ClientFetch (when starting thread) and GUI, used for changing camera mode*/
	public synchronized void modeSwitch(int newMode, int camPort){
		if (camPort == 0){
			this.hostName = "";
		}
		else {
			switch(camPort/1000){
			case 6:
				hostName = camHost1;
				break;
			case 7:
				hostName = camHost2;
				break;
			case 8:
				hostName = camHost3;
				break;
			case 9:
				hostName = camHost4;
				break;
			default: break;	
			}
		}
		mode=newMode;
		modeFlag = true;
		notifyAll();
	}

	/** Set to sync mode */
	public synchronized void syncMode(){
		syncMode = syncForced;
	}

	/** Set to async mode */
	public synchronized void asyncMode(){
		syncMode = asyncForced;
	}

	/** Set to auto sync mode */
	public synchronized void autoSyncMode(){
		syncMode = syncAuto;
		sync=true;
	}

	// *********************** Commands for connect/disconnect cameras *************************

	/** Disconnect both cams */
	public synchronized void disconnect(String location){
		if(location.equals("Left")){
			connectLeft = false;
		}else{
			connectRight = false;
		}
		notifyAll();
	}

	/** Connect both cams */
	public synchronized void connect(){
		connectRight = true;
		connectLeft = true;
		notifyAll();
	}

	/** Called from GUI, telling one ClientSend and the matching ClientFetch to change cam */
	public synchronized void changeCam(String location, String cam){
		int tempFetch = 0;
		int tempSend = 0;
		String tempHost = "";
		
		boolean illegal = false;

		if (location.equals("Right")){
			if(leftHost==cam){ illegal = true; }
		} else {
			if(rightHost==cam){ illegal = true;}
		}

		if (!illegal){
			disconnect(location);

			switch (cam){
			case "argus-1.student.lth.se":
				tempFetch = fetch1;
				tempSend = send1;
				tempHost = camHost1;
				break;
			case "argus-2.student.lth.se":
				tempFetch = fetch2;
				tempSend = send2;
				tempHost = camHost2;
				break;
			case "argus-3.student.lth.se":
				tempFetch = fetch3;
				tempSend = send3;
				tempHost = camHost3;
				break;
			case "argus-4.student.lth.se":
				tempFetch = fetch4;
				tempSend = send4;
				tempHost = camHost4;
				break;
			}
			if (location.equals("Right")){
				rightFetch = tempFetch;
				rightSend = tempSend;
				rightHost = tempHost;
				connectRight = true;
			} else if (location.equals("Left")){
				leftFetch = tempFetch;
				leftSend = tempSend;
				leftHost = tempHost;
				connectLeft = true;
			}
			notifyAll();
		}
	}

	/** Called by ClientSend and ClientFetch to get its port nr. Sort=Fetch or Send*/
	public synchronized int getPort(String sort, String location){
		int port = 0;
		while((!connectLeft && location.equals("Left")) || (!connectRight && location.equals("Right"))){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (location.equals("Left")){
			if (sort.equals("Fetch")){
				port=leftFetch;
				System.out.println("Fetch " + port + " " + location);
			} else if (sort.equals("Send")){
				port=leftSend;
				System.out.println("Send " + port + " " + location);
			}
			portInc++;
			if(portInc==2){
				portInc=0;
				//				connectLeft = false;
			}
		} else if (location.equals("Right")){
			if (sort.equals("Fetch")){
				port=rightFetch;
				System.out.println("Fetch " + port + " " + location);
			} else if (sort.equals("Send")){
				port=rightSend;
				System.out.println("Send " + port + " " + location);
			}
			portInc++;
			if(portInc==2){
				portInc=0;
				//				connectRight = false;
			}
		}
		return port;
	}

	// *********************** Private methods ***************************

	/** For converting int to byte[] */
	private byte[] intToByte(int value){
		return new byte[] {
				(byte)(value >>> 24),
				(byte)(value >>> 16),
				(byte)(value >>> 8),
				(byte)value};
	}
}
