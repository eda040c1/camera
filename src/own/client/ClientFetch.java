package own.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;


public class ClientFetch extends Thread {

	/** Server modes */
	private static final int IDLE = 0;
	private static final int MOVIE = 1;

	private ClientMonitor monitor;

	/** Socket communication */
	private boolean connected;
	private Socket socket;
	private int port;
	private InputStream in;
	private OutputStream out;
	private String host = "localhost";
	private String location;
	private byte mode;

	private byte[] packageLength;

	private byte[] jpeg;

	/** For reading data from ServerSend */
	private int size;
	private int read;
	private int i;
	private int n;

	/** For reading first byte and sending ack */ 
	private byte first;
	private byte ACK;

	public ClientFetch(ClientMonitor monitor, String location){
		this.monitor = monitor;
		this.location = location;
		size = 0;
		packageLength = new byte[4];
	}

	public void run(){
		while (true){
			try {
				
				port = monitor.getPort("Fetch", location);
				
				// Used for telling client which mode server is in if client has crashed and been restarted 
				//Server must be running
				socket = new Socket(host, port);
				// Get input & output
				in = socket.getInputStream();
				out = socket.getOutputStream();

				System.out.println("Before read");
				mode = (byte)in.read(); 
				System.out.println("After read");
				if (mode == (byte) 0){
					monitor.modeSwitch(IDLE, 0);
				} else if (mode == (byte) 1){
					monitor.modeSwitch(MOVIE, 0);
				}
				ACK = 1;
				out.write(ACK);
				out.flush();		//Tvingar ut det som ligger kvar i write.
				
				connected = true;
				
				System.out.println("In while");

				while (connected){

					/*
					 * MOTION DETECTED IF = 0
					 */
					first = (byte)in.read(); //header antingen 0 eller 1

					/*
					 * Plockar ut f�rsta byten ur paketet och stoppar 
					 * in det i packageLength f�r att ta reda p� storleken.
					 */
					read = 0;
					while(read != 4){
						n = in.read(packageLength, read, 4-read);
						if(n == -1){
//							System.out.println("n==-1");
							throw new IOException("ClientFetch: End of stream");
						}
						read += n;
					}

					/*Convert byte vector "packageLength" to a numeric value*/
					size = toInt(packageLength);

					// If first == 1 jpeg
					if (first == 1){

						/*Skapar en ny byte-vektor med l�ngden av hela paketet */
						jpeg = new byte[Long.BYTES + size];

						//Read package
						read = 0;
						i = 0;
						while (read != size-4){
							//Reads bytes and puts in data
							//						System.out.println("in available:  "+in.available() + "Size:  "+size);
							n = in.read(jpeg, read, size-read-4);
//													System.out.println("Value of in.read(): "+n);
							i++;
							if(n == -1){
//								System.out.println("n==-1");
								throw new IOException("ClientFetch: End of stream");
							}
							read += n;
							//						System.out.println("Read: "+read+" n: "+n);
						}
//											System.out.println("Left while. n=" + read);
						connected = monitor.putData(port, jpeg, size);
					}
					// If read returns -1 then end-of-stream has been reached
					else if(first == -1){
						throw new IOException("ClientFetch: End of stream");
					}
					// If first == 0 motion detection
					else if(first == 0){
						monitor.modeSwitch(MOVIE, port); // number 1 = movie
					}

					ACK = 1;
					out.write(ACK);
					out.flush();		//Tvingar ut det som ligger kvar i write.
					
					if(!connected){
						socket.close();
					}
				}
			} catch (UnknownHostException e) {
				// Occurs if the socket cannot find the host
				e.printStackTrace();
			} catch (IOException e) {
				// Occurs if there is an error trying to connect to the host,
				// or there is an error during the call to the write method.
				//
				// Example: the connection is closed on the server side, but
				// the client is still trying to write data.
				e.printStackTrace();
			} finally {
				//			try {
				//				socket.close();		//!!!!!!!!!!!!!!! Will this work
				//			} catch (IOException e) {
				//				// TODO Auto-generated catch block
				//				e.printStackTrace();
				//			}
			}
		}
	}

	// ********************** Private method ***********************

	private int toInt(byte[] bytes) {
		int ret = 0;
		for (int i=0; i<4 && i<bytes.length; i++) {
			ret <<= 8;
			ret |= (int)bytes[i] & 0xFF;
		}
		return ret;
	}
}
