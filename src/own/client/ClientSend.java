package own.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientSend extends Thread{
	private Socket socket;
	private int port;
	private InputStream in;
	private OutputStream out;
	private ClientMonitor monitor;
	private byte[] motionByte;
	private int size;
	private boolean connected;
	private String host = "localhost";
	private String location;
	private int ACK;

	public ClientSend(ClientMonitor monitor, String location){
		this.monitor = monitor;
		this.location = location;
		motionByte = new byte[4];
	}

	public void run(){
		try{
			while(true){
				// Increase the probability that the server is accepting connections,
				// it is *still* a race condition though
				Thread.sleep(2000);	//Semaphore instead (not possible), While solution??

				port = monitor.getPort("Send", location);

				//Server must be running
				socket = new Socket(host, port);

				in = socket.getInputStream();
				out = socket.getOutputStream();

				connected = true;
				
				// To tell camera which mode it should start in
				motionByte = monitor.startMode();
				size = motionByte.length;
				
				// Send package
				out.write(motionByte, 0, size);

				// Flush data
				out.flush();

				// Wait for acknowledgment - read is blocking
				ACK = in.read();
				// If read returns -1 then end-of-stream has been reached
				if (ACK == -1) {
					throw new IOException("ClientSend: End of stream");
				}
				
				while(connected){

					motionByte = monitor.changeMode(port, location);

					if (motionByte == null){
						connected = false;
					} else {
						size = motionByte.length;						

						// Send package
						out.write(motionByte, 0, size);
						
						// Flush data
						out.flush();

						// Wait for acknowledgment - read is blocking
						ACK = in.read();
						// If read returns -1 then end-of-stream has been reached
						if (ACK == -1) {
							throw new IOException("ClientSend: End of stream");
						}
						System.out.println("ClientSend: Ack recived");

						// Close the socket, i.e. abort the connection
						//socket.close();
						//System.out.println("ClientFetch: Socket closed");

					}
				}
			}

		} catch (UnknownHostException e) {
			// Occurs if the socket cannot find the host
			e.printStackTrace();
		} catch (IOException e) {
			// Occurs if there is an error trying to connect to the host,
			// or there is an error during the call to the write method.
			//
			// Example: the connection is closed on the server side, but
			// the client is still trying to write data.
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

