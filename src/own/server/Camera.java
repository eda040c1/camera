package own.server;

//import se.lth.cs.eda040.realcamera.AxisM3006V;
import se.lth.cs.eda040.fakecamera.AxisM3006V;
//import se.lth.cs.eda040.proxycamera.AxisM3006V;



public class Camera extends Thread{
	private ServerMonitor monitor = null;
	private AxisM3006V myCamera = null;
	private int port = 0;
	private byte[] jpeg=null;
	private int size = 0;
	private long timeStamp = 0;

	public Camera(String hostName, int port, ServerMonitor monitor){
		this.monitor = monitor;
		myCamera = new AxisM3006V();
		myCamera.init();
		myCamera.setProxy(hostName, port);
		myCamera.connect();
		
		jpeg = new byte[AxisM3006V.IMAGE_BUFFER_SIZE]; //size? too large?
		//		myCamera.setProxy("argus-1.student.lth.se", port);
		this.port = port;
	}

	public void run(){
		while(true){
			if(myCamera.motionDetected() && monitor.idleMode()){
				monitor.motionDetection();
			} else {
				if(monitor.idleMode()){
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				size = myCamera.getJPEG(jpeg, 0);
				timeStamp = System.currentTimeMillis();
//				System.out.println("Axis Size:  "+ AxisM3006V.IMAGE_BUFFER_SIZE + "\n" + "Size:  "+size);
				monitor.takePicture(jpeg, size, timeStamp);
			}
		}
	}
}
