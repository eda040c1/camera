package own.server;
//import se.lth.cs.eda040.realcamera.AxisM3006V;

//import se.lth.cs.eda040.proxycamera.AxisM3006V;
import se.lth.cs.eda040.fakecamera.AxisM3006V;

public class ServerMonitor {

	/** Server modes */
	private int mode = 0; // 0 = idle, 1 = movie

	private byte data[];

	/** synchronized void motionDetection() */
	byte[] motionByte = null;

	/** synchronized void changeMode() */
	byte[] modeByte  = null;

	/** Camera flag used when notifying serverSend about new picture*/
	private boolean updateFlag = false;
	private boolean motionFlag = false;

	/** Arrays used in methods */
	private byte[] longToByteArray = null;
	private byte[] addArraysArray = null;

	public ServerMonitor(){
		data = new byte[4 + 8 + AxisM3006V.IMAGE_BUFFER_SIZE];
		updateFlag = false;
		motionFlag=false;
		motionByte = new byte[4];
		modeByte = new byte[4];
	}

	/**Used in Camera for updating Picture */
	public synchronized void takePicture(byte[] jpeg, int size, long timeStamp){
		data = addArrays(intToByte(size +8 + 4), longToByte(timeStamp), jpeg, size);
		updateFlag = true;
		notifyAll();
	}

	/**Used in Camera if motion is detected */
	public synchronized void motionDetection(){
		motionByte = intToByte(1);
		data = motionByte;
		mode = 1;		//movie
		motionFlag = true;
		notifyAll();
	}

	/** Used by ServerSend to get new data to send to client */
	public synchronized byte[] getData(){
		//Wait if nothing to send
		while(!updateFlag && !motionFlag){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(updateFlag){updateFlag = false;}
		if(motionFlag){motionFlag = false;}

		notifyAll();
		return data;
	}

	/** Used by ServerFetch to change camera mode */
	public synchronized void changeMode(byte[] data){
		mode = 	(data[0]<<24)&0xff000000|
				(data[1]<<16)&0x00ff0000|
				(data[2]<< 8)&0x0000ff00|
				(data[3]<< 0)&0x000000ff;
	}

	/** Used for telling camera if it's in idle mode or not */
	public synchronized boolean idleMode(){
		return mode == 0;
	}

	// *********************** Private methods ***************************

	/** For converting int to byte[] */
	public byte[] intToByte(int value) {
		return new byte[] {
				(byte)(value >>> 24),
				(byte)(value >>> 16),
				(byte)(value >>> 8),
				(byte)value};
	}

	/** For converting long to byte[] */
	private byte[] longToByte(long num){
		longToByteArray = new byte[8];

		int offset;
		for(int i = 0; i < 8; i++){
			offset = (longToByteArray.length - 1 - i) * 8;
			longToByteArray[i] = (byte) ((num >>> offset) & 0xFF);
		}
		return longToByteArray;
	}

	/** Creates 1 byte[] from a, b and c */
	private byte[] addArrays(byte[] a, byte[] b, byte[] c, int imgSize){
		addArraysArray = new byte[a.length + b.length + imgSize];
		
		int i;
		for(i=0; i<a.length; i++){
			addArraysArray[i] = a[i];
		}

		for(i=a.length; i<(a.length+b.length); i++){
			addArraysArray[i] = b[i-a.length];
		}

		for(i=(a.length+b.length); i<(a.length + b.length + imgSize); i++){
			addArraysArray[i] = c[i-(a.length+b.length)];
		}
		return addArraysArray;
	}
}
