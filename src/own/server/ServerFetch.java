package own.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerFetch extends Thread {
	private ServerSocket serverSocket = null;
	private Socket socket = null;
	private InputStream in = null;
	private OutputStream out = null;
	private byte[] modeByte = null;
	private int port = 0;
	private ServerMonitor monitor  = null;
	private int read = 0;
	private int size = 0;
	private byte ACK = 0;
	private boolean connection = false;

	public ServerFetch(int port, ServerMonitor monitor){
		this.monitor = monitor;
		modeByte = new byte[4];		
		//		try {
		this.port = port;
		socket = null;
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			//		System.out.println("FetchServer could not create a socket at: " + port);
		//			e.printStackTrace();
		//		}
	}

	public void run(){
		while(true){
			try{
				serverSocket = new ServerSocket(port);
				//Blocking
				socket = serverSocket.accept();
				//socket.setTcpNoDelay(true);
				in = socket.getInputStream();
				out = socket.getOutputStream();
				connection = true;

				try{
					while(connection){
						size=modeByte.length;
						read = 0;
						while(read != size){
							int n = in.read(modeByte, read, size-read);
							if (n == -1){
								throw new IOException("ServerFetch: End of stream");
							}
							read += n;
						}
						System.out.println("ServerFetch: go in to monitor");
						monitor.changeMode(modeByte);

						// Send back a byte indicating acknowledgment
						ACK = 1;
						out.write(ACK);
						out.flush();
						System.out.println("ServerFetch: ACK sent");
					}
					socket.close();
					serverSocket.close();

				} catch (IOException e) {
					//			System.out.println("ServerFetch: The connection has been aborted.");
					connection = false;
					socket.close();
					serverSocket.close();

					e.printStackTrace();
				} 
			} catch (IOException e) {
				//		System.out.println("ServerFetch: Exception while closing server socket.");
				connection = false;
				e.printStackTrace();
			}
		}
	}
}
