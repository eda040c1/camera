package own.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class ServerSend extends Thread {
	private int port=0;
	private Socket socket=null;
	private ServerSocket serverSocket=null;
	private ServerMonitor monitor=null;
	private byte[] jpeg = null;
	private byte first = 0;
	private int ack = 0; 
	private boolean connection = false;

	private InputStream in;
	private OutputStream out;

	public ServerSend(int port, ServerMonitor monitor){
		this.port = port;
		this.monitor = monitor;
	}

	public void run(){
		while(true){
			try{

				serverSocket = new ServerSocket(port);
				socket=serverSocket.accept();

				in = socket.getInputStream();
				out = socket.getOutputStream();

				connection = true;
				
				try{

					// Used for telling client which mode server is in if client has crashed and been restarted 
					byte mode;
					if(monitor.idleMode()){
						mode = 0;
					} else {
						mode = 1;
					}
					out.write(mode);
					out.flush();
					// Wait for acknowledgment - read is blocking
					ack = in.read();
					// If read returns -1 then end-of-stream has been reached
					if (ack == -1) throw new IOException("End of stream");
					System.out.println("ServerSend: Going in while");

					while(connection) {

						//Update picture
						jpeg = monitor.getData();

						/**If size is <= Integer.BYTES, the package is just a move alert and sends a 0 as first header.
						If size is bigger it is a pic package and then the first header is a 1. 
						This is for notifying the Client Fetch about what sort of data it will recive*/


						if(jpeg.length <= 4){
							first = (byte) 0;
						} else {
							first = (byte) 1;
						}

//						System.out.println("ServerSend: Write ");

						// Send first header
						out.write(first);

						// Send package
						out.write(jpeg, 0, jpeg.length);

						// Flush data
						out.flush();

						// Wait for acknowledgment - read is blocking
						ack = in.read();
						// If read returns -1 then end-of-stream has been reached
						if (ack == -1) throw new IOException("End of stream");
//						System.out.println("ServerSend: Received ack from connection " + port);
					}
				} catch (UnknownHostException e) {
					// Occurs if the socket cannot find the host

					socket.close();
					serverSocket.close();

					connection = false;
					e.printStackTrace();

				} catch (IOException e) {
					// Occurs if there is an error trying to connect to the host,
					// or there is an error during the call to the write method.
					//
					// Example: the connection is closed on the server side, but
					// the client is still trying to write data.
					connection = false;

					socket.close();
					serverSocket.close();

					e.printStackTrace();
				}
				//				socket.close();
				//				serverSocket.close();

			} catch (IOException e) {
				// Occurs if there is an error trying to connect to the host,
				// or there is an error during the call to the write method.
				//
				// Example: the connection is closed on the server side, but
				// the client is still trying to write data.
				connection = false;
				e.printStackTrace();
			}
		}
	}
}
