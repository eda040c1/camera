import own.server.Camera;
import own.server.ServerFetch;
import own.server.ServerMonitor;
import own.server.ServerSend;

public class mainServer2 {

	public static void main(String[] args) {
		//Must start first
		ServerMonitor monitor = new ServerMonitor();

		int portR = 7001;
		int portS = 7000;
		
		String host = "argus-2.student.lth.se";
		
		ServerFetch serverFetch = new ServerFetch(portR, monitor);
		ServerSend serverSend = new ServerSend(portS, monitor);
		Camera cam = new Camera(host, portS, monitor);


		cam.start();

		serverFetch.start();
		/*try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		serverSend.start();
	}
}


