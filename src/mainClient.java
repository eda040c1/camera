import own.client.*;
import gui.*;

public class mainClient {

	public static void main(String[] args) {

		ClientMonitor monitor = new ClientMonitor();

		ClientSend clientSendLeft = new ClientSend(monitor, "Left");
		ClientSend clientSendRight = new ClientSend(monitor, "Right");
		
		ClientFetch clientFetchLeft = new ClientFetch(monitor, "Left");
		ClientFetch clientFetchRight = new ClientFetch(monitor, "Right");

		GraphicalUI gui = new GraphicalUI(monitor);

		(new Thread(gui)).start();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		clientSendLeft.start();
		clientSendRight.start();
		/*try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		clientFetchLeft.start();
		clientFetchRight.start();
	}
}
