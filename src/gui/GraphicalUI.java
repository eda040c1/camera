package gui;

import java.awt.BorderLayout;
//import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
//import java.util.concurrent.Semaphore;

import javax.swing.*;
import own.client.ClientMonitor;

public class GraphicalUI extends JFrame implements Runnable {

	ImageIcon icon;
	ImageIcon icon2;
	ClientMonitor cm;

	// JLabels, Layouts etc.
	private BorderLayout board;
	private CommandPanel commandPanel;
	private HeadPanel headPanel;

	private JLabel image;
	private JLabel image2;
	private JLabel modeLabel;

	private ArrayList<byte[]> temp;
	private Dimension minimumSize = new Dimension(640, 480);

	private String tmpMode;
	private String currentMode;
	private String leftHost="";
	private String rightHost="";

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */

	public GraphicalUI(ClientMonitor cm) {
		// Create and set up the window.

		super("Camera viewer");
		this.cm = cm;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		tmpMode = cm.cameraMode();
		currentMode = tmpMode;

		board = new BorderLayout();
		commandPanel = new CommandPanel(this, cm);
		getContentPane().setLayout(board);

		headPanel = new HeadPanel(this, cm);

		icon = new ImageIcon();
		icon.getClass().getResource("/images/disconnected.png");

		icon2 = new ImageIcon();
		icon2.getClass().getResource("/images/disconnected.png");

		image = new JLabel(icon);
		image2 = new JLabel(icon2);

		image.setMinimumSize(minimumSize);
		image2.setMinimumSize(minimumSize);

		modeLabel = new JLabel(Mode.IDLE.toString()); // Indicates current mode

		/*
		 * Adding a drop down menu for choosing cameras to displays
		 */
		JMenu cameraMenuLeft = new JMenu("Left View");
		JMenu cameraMenuRight = new JMenu("Right View");
		JMenuItem[] menuItemsLeft = new JMenuItem[4];
		JMenuItem[] menuItemsRight = new JMenuItem[4];
		for (int i = 0; i < 4; i++) {
			menuItemsLeft[i] = new JMenuItem("Camera " + (i + 1));
			menuItemsRight[i] = new JMenuItem("Camera " + (i + 1));
			menuItemsLeft[i].addActionListener(new MenuListener(i+1));
			menuItemsRight[i].addActionListener(new MenuListener(i+1+4));
			cameraMenuRight.add(menuItemsRight[i]);
			cameraMenuLeft.add(menuItemsLeft[i]);
		}
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(cameraMenuLeft);
		menuBar.add(cameraMenuRight);
		setJMenuBar(menuBar);

		add(headPanel, BorderLayout.PAGE_START);
		add(commandPanel, BorderLayout.PAGE_END);

		// Display the window.
		pack();
		setSize(1280, 740);
		setVisible(true);

	}

	public void run() {
		while (true) {
			long time = System.currentTimeMillis();
			temp = cm.getData();
			refreshImage(0, temp); // Must take care of from which cam, 0 or 1
			// and refresh to right display
			refreshImage(1, temp); // Cam 2
			commandPanel.delayedTime();
			commandPanel.syncMode(cm.isSync(), cm.isAuto());
		}
	}

	private void refreshImage(int source, ArrayList<byte[]> stream) {

		tmpMode = cm.cameraMode();
		//		System.out.println(tmpMode);

		if (source == 0) {
			if (stream.get(source) == null) {
				icon.getClass().getResource("/images/disconnected.png");
			} else {
				Image image = getToolkit().createImage(stream.get(source));
				getToolkit().prepareImage(image, -1, -1, null);
				icon.setImage(image);
				icon.paintIcon(this, this.getGraphics(), 0, 70);
			}
			if (!(currentMode.equals(tmpMode))) {
				System.out.println(modeLabel.getText());
				headPanel.switchModeLabel(tmpMode);

				if (currentMode.equals("Idle")) {
					System.out.println("Idle to movie");
					JOptionPane.showMessageDialog(this, "Motion detetcted on "+leftHost+" ! \n Movie mode activated");
					
				} else {

					System.out.println("Movie to idle");
					JOptionPane.showMessageDialog(this, "Surveillance mode changed to Idle");
				}
			}
			currentMode = tmpMode;
		}
		if (source == 1) {

			if (stream.get(source) == null) {
				icon2.getClass().getResource("/images/disconnected.png");
			} else {
				Image image2 = getToolkit().createImage(stream.get(source));
				getToolkit().prepareImage(image2, -1, -1, null);
				icon2.setImage(image2);
				icon2.paintIcon(this, this.getGraphics(), 15 + 640, 70);
			}
			if (!(currentMode.equals(tmpMode))) {
				System.out.println(modeLabel.getText());
				headPanel.switchModeLabel(tmpMode);
				if (currentMode.equals("Idle")) {
					System.out.println("Idle to movie");
					JOptionPane.showMessageDialog(this, "Motion detetcted on "+rightHost+"! \n Movie mode activated");

				} else {

					System.out.println("Movie to idle");
					JOptionPane.showMessageDialog(this, "Surveillance mode changed to Idle");
				}
			}
		}
	}

	class MenuListener implements ActionListener {
		private int cam;

		public MenuListener(int cam) {
			this.cam = cam;
		}

		public void actionPerformed(ActionEvent e) {

			switch (cam) {
			case 1:
				System.out.println("Left view switched to Camera 1");
				cm.changeCam("Left", "argus-1.student.lth.se");
				break;
			case 2:
				System.out.println("Left view switched to Camera 2");
				cm.changeCam("Left", "argus-2.student.lth.se");
				break;
			case 3:
				System.out.println("Left view switched to Camera 3");
				cm.changeCam("Left", "argus-3.student.lth.se");
				break;
			case 4:
				System.out.println("Left view switched to Camera 4");
				cm.changeCam("Left", "argus-4.student.lth.se");
				break;
			case 5:
				System.out.println("Right view switched to Camera 1");
				cm.changeCam("Right", "argus-1.student.lth.se");
				break;
			case 6:
				System.out.println("Right view switched to Camera 2");
				cm.changeCam("Right", "argus-2.student.lth.se");
				break;
			case 7:
				System.out.println("Right view switched to Camera 3");
				cm.changeCam("Right", "argus-3.student.lth.se");
				break;
			case 8:
				System.out.println("Right view switched to Camera 4");
				cm.changeCam("Right", "argus-4.student.lth.se");
				break;
			default:
				break;
			}
		}
	}
}
