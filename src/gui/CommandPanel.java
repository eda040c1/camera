package gui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import own.client.ClientMonitor;

public class CommandPanel extends JPanel {
	private ClientMonitor cm;
	private JLabel currentMode;
	private GuiActionListener gal;
	private JLabel delay1;
	private JLabel delay2;
	private JLabel modeLabel;
	private JPanel leftPanel;
	private JPanel rightPanel;
	private JPanel bottomPanel;
	private JPanel syncPanel;

	/** Server modes */
	static final int IDLE = 0;
	static final int MOVIE = 1;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CommandPanel(GraphicalUI ui, ClientMonitor cm) {

		this.cm = cm;

		currentMode = new JLabel(""); // Should fetch String
		gal = new GuiActionListener();
		setLayout(new GridLayout(0,2));
		leftPanel = new JPanel();
		rightPanel = new JPanel();
		bottomPanel = new JPanel();
		

		leftPanel.setLayout(new GridLayout(0,2));
		rightPanel.setLayout(new GridLayout(0,2));
		bottomPanel.setLayout(new GridLayout(2,5));
		
		
		JButton switchMode = new JButton("Idle");
		JButton switchMode2 = new JButton("Movie");
		JButton disconnect = new JButton("Disconnect Left Camera");
		JButton disconnect2 = new JButton("Disconnect Right Camera");
		JButton sync = new JButton("Synchronos");
		JButton aSync = new JButton("Asynchronos");
		JButton autoSync = new JButton("Auto");
		JLabel modePres = new JLabel("Current mode: ");
		
		switchMode.addActionListener(gal);
		switchMode2.addActionListener(gal);
		disconnect.addActionListener(gal);
		disconnect2.addActionListener(gal);
		sync.addActionListener(gal);
		aSync.addActionListener(gal);
		autoSync.addActionListener(gal);

		delay1 = new JLabel("Delay:");
		delay2 = new JLabel("Delay:");

		// Left to right button and JLabel display
		leftPanel.add(delay1);
		leftPanel.add(disconnect);
		rightPanel.add(delay2);
		rightPanel.add(disconnect2);
		bottomPanel.add(modePres);
		bottomPanel.add(currentMode);
		bottomPanel.add(switchMode2);
		bottomPanel.add(switchMode);
		bottomPanel.add(sync);
		bottomPanel.add(aSync);
		bottomPanel.add(autoSync);

		Border leftCam = BorderFactory.createTitledBorder("Left Camera Settings");
		leftPanel.setBorder(leftCam);

		Border rightCam = BorderFactory.createTitledBorder("Right Camera Settings");
		rightPanel.setBorder(rightCam);
		
		Border syncMenu = BorderFactory.createTitledBorder("Synchronos settings");
		bottomPanel.setBorder(syncMenu);

		add(leftPanel);
		add(rightPanel);
		add(bottomPanel);
	}

	public synchronized void syncMode(boolean isSync, boolean isAuto) {
		String sync = new String();
		String auto = new String();
		if (isSync){
			sync = Mode.Sync.toString();
			if(isAuto){ 
				auto = "Auto";
			} else {
				auto = "Forced";
			}
		} else {
			sync = Mode.Async.toString();
			if(isAuto){ 
				auto = "Auto";
			} else {
				auto = "Forced";
			}
		}
		currentMode.setText(auto + " " + sync);
		currentMode.repaint();
		

	}

	private class GuiActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			switch (e.getActionCommand()) {
			default:
				break;
			case "Idle":
				cm.modeSwitch(IDLE, 0);
				modeLabel = new JLabel("Idle");
				modeLabel.repaint();
				break;
			case "Movie":
				cm.modeSwitch(MOVIE, 0);
				modeLabel = new JLabel("Movie");
				modeLabel.repaint();
				break;
			case "Disconnect Left Camera":
				System.out.println("Camera 1 disconnected");
				cm.disconnect("Left");
				break;
			case "Disconnect Right Camera":
				cm.disconnect("Right");
				System.out.println("Camera 2 disconnected");
				break;
			case "Synchronos":
				cm.syncMode();
				System.out.println("Synchronos");
				break;
			case "Asynchronos":
				cm.asyncMode();
				System.out.println("Asynchronos");
				break;
			case "Auto":
				cm.autoSyncMode();
				System.out.println("AutoSync");
				break;
			}
		}
	}

	public synchronized void delayedTime() {
		delay1.setText("Delay:" + " " + cm.getDelay(0) + " s");
		delay1.repaint();
		delay2.setText("Delay:" + " " + cm.getDelay(1) + " s");
		delay2.repaint();
	}

}
