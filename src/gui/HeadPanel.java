package gui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import own.client.ClientMonitor;

public class HeadPanel extends JPanel {
	private ClientMonitor cm;
	private String tmpMode;
	private JLabel leftCam;
	private JLabel rightCam;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HeadPanel(GraphicalUI ui, ClientMonitor cm) {
		this.cm = cm;
	
		setLayout(new GridLayout(0,2));
		
		leftCam = new JLabel("Left Camera \t " + cm.cameraMode() + "\t" + cm.hostName(""));
		rightCam = new JLabel("Right Camera \t " + cm.cameraMode() + "\t" + cm.hostName(""));

		
		leftCam.setLayout(new FlowLayout());
		rightCam.setLayout(new FlowLayout());
		
		add(leftCam);
		add(rightCam);
	}
	public synchronized void switchModeLabel(String newMode){
		leftCam.setText("Left Camera \t " + cm.cameraMode() + "\t" + cm.hostName("Left"));
		rightCam.setText("Right Camera \t " + cm.cameraMode() + "\t" + cm.hostName("Right"));
		leftCam.repaint();
		rightCam.repaint();
	}
}
