import own.server.ServerMonitor;
import own.server.ServerFetch;
import own.server.ServerSend;
import own.server.Camera;

public class mainServer {

	public static void main(String[] args) {
		//Must start first
		ServerMonitor monitor = new ServerMonitor();
		
		int portR = 6001;
		int portS = 6000;
		
		String host = "argus-1.student.lth.se";

		
		ServerFetch serverFetch = new ServerFetch(portR, monitor);
		ServerSend serverSend = new ServerSend(portS, monitor);
		Camera cam = new Camera(host, portS, monitor);
		
		cam.start();
		
		serverFetch.start();
		/*try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		serverSend.start();
	}
}
